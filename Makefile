initialize:
	npm install -g spago@0.13.0 purescript@0.13.5 cordova@9.0.0
	npm install
	mkdir platforms
	mkdir plugins
	cordova platform add android
	cordova plugin add cordova-plugin-flashlight
	cordova plugin add cordova-sqlite-storage

build:
	cd app/ \
	&& node-sass src/styles/index.scss > ../www/css/index.css \
	&& spago bundle-app -t ../www/js/app.js

serve: build
	cordova serve

pscid: 
	cd app/ \
	&& pscid

deploy: build
	cordova run android
