module View.REM where

import Prelude
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Number (fromString)
import Effect.Aff (killFiber, forkAff, Aff, delay, Milliseconds(..))
import Effect.Timer
import Effect.Exception (error)
import Halogen.Query.EventSource (EventSource)
import Halogen.Query.HalogenM (SubscriptionId)
import Halogen.Query.EventSource as EventSource
import Control.Monad.Rec.Class (forever)

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Core as HC
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP

import DOM.HTML.Indexed.InputType (InputType(..))

import Components as C
import Cordova.Flashlight as FL
import Cordova as Cord

import Debug.Trace (spy)

type Slot = forall q. H.Slot q Void Unit

data FlashState
  = Sleep Number
  | Repetition Int
  | Stopped

data SubView
  = EditView
  | InfoView

type State =
  { subId :: Maybe SubscriptionId
  , state :: FlashState
  , tmpTimeout :: String
  , tmpInterval :: String
  , tmpDuration :: String
  , timeout :: Number
  , interval :: Number
  , duration :: Number
  , activeView :: SubView
  }

data Action
  = ToggleRem
  | Timer
  | Edit
  | Save
  | Back
  | ClearTimeout
  | ClearInterval
  | ClearDuration
  | SetTimeout String
  | SetInterval String
  | SetDuration String

component :: forall q i o m. H.Component HH.HTML q i o Aff
component =
  H.mkComponent
    { initialState
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }

initialState :: forall i. i -> State
initialState _ =
  { subId: Nothing
  , state: Stopped
  , tmpTimeout: ""
  , tmpInterval: ""
  , tmpDuration: ""
  , timeout: 4.0
  , interval: 1.0
  , duration: 1.0
  , activeView: InfoView
  }

render :: forall m. State -> H.ComponentHTML Action () Aff
render s = case s.activeView of
  InfoView -> renderInfoView
  EditView -> renderEditView

  where
    renderInfoView =
      HH.div
      [ ]
      [ C.header
        $ C.ActionHeader
          "REMulator"
          { icon: "edit", action: Just Edit }
      , C.content
        [ HH.div
          [ HP.class_ $ H.ClassName "fl pb3 w-100" ]
          [ HH.text "REMulator will start in "
          , HH.text $ show s.timeout
          , HH.text " hour and will toggle the light in an interval of "
          , HH.text $ show s.interval
          , HH.text $ if s.interval > 1.0 then " seconds for " else " second for "
          , HH.text $ show s.duration
          , HH.text $ if s.duration > 1.0 then " minutes" else " minute"
          ]
        , C.button
          { label: case s.state of
              Stopped -> "Start"
              _ -> "Stop"
          , action: Just ToggleRem
          }
        ]
      ]

    renderEditView =
      HH.div
      [ ]
      [ C.header
        $ C.SubViewHeaderAction
          "Edit"
          (Just Back)
          { icon: "save"
          , action: Just Save
          }
      , C.content
        [ C.input
          { placeholder: "Timeout: in hours (default: 4)"
          , value: s.tmpTimeout
          , type_: InputNumber
          , onInput: \s -> Just $ SetTimeout s
          , clear: Just ClearTimeout
          }
        , C.input
          { placeholder: "Interval: in seconds (default: 1)"
          , value: s.tmpInterval
          , type_: InputNumber
          , onInput: \s -> Just $ SetInterval s
          , clear: Just ClearInterval
          }
        , C.input
          { placeholder: "Duration: in minutes (default: 1)"
          , value: s.tmpDuration
          , type_: InputNumber
          , onInput: \s -> Just $ SetDuration s
          , clear: Just ClearDuration
          }
        ]
      ]

handleAction :: forall o m. Action -> H.HalogenM State Action () o Aff Unit
handleAction = case _ of
  Back -> do
    H.modify_ (_ { activeView = InfoView, tmpTimeout = "", tmpInterval = "" })

  Save -> do
    st <- H.get
    let to = fromString st.tmpTimeout
        it = fromString st.tmpInterval

    H.modify_ \s ->
      s{ activeView = InfoView
       , timeout = fromMaybe 4.0 to
       , interval = fromMaybe 1.0 it
       }

  Edit -> do
    H.modify_ (_ { activeView = EditView })

  ClearTimeout -> do
    H.modify_ (_ { tmpTimeout = "" })

  ClearInterval -> do
    H.modify_ (_ { tmpInterval = "" })

  SetTimeout s -> do
    H.modify_ (_ { tmpTimeout = s })

  ClearDuration -> do
    H.modify_ (_ { tmpDuration = "" })

  SetDuration s -> do
    H.modify_ (_ { tmpDuration = s })

  SetInterval s -> do
    H.modify_ (_ { tmpInterval = s })

  ToggleRem -> do
    state <- H.get
    case state.state of
      Stopped -> do
        s <- H.get
        fl <- H.liftAff FL.available
        if fl
          then do
            sid <- H.subscribe timer
            H.modify_ (_ { state = Sleep s.timeout , subId = Just sid })
          else do
            _ <- H.liftEffect $ Cord._alert "No Flashlight available"
            pure unit

      _ -> do
        case state.subId of
          Nothing -> pure unit
          Just s -> do
            _ <- H.unsubscribe s
            pure unit
        _ <- H.liftAff $ FL.switchOff
        H.modify_ (_ { state = Stopped, subId = Nothing })

  Timer -> do
    s <- H.get

    case s.state of
      Sleep time -> do
        _ <- H.liftAff $ delay $ Milliseconds time
        H.modify_ (_ { state = Repetition 0 })
      Repetition i -> do
        on <- H.liftEffect $ FL.isSwitchedOn

        if on
          then do
            _ <- H.liftAff FL.switchOff
            H.modify_ (_ { state = Repetition $ i+1 })
          else
            H.liftAff $ FL.switchOn

      Repetition 11 -> do
        _ <- H.liftAff FL.switchOff
        H.modify_ (_ { state = Sleep $ toHour 0.5 })

      Stopped ->
        pure unit

toHour v = v * 60.0 * 60.0 * 60.0 * 1000.0

toSeconds v = v * 60.0 * 1000.0

timer :: EventSource Aff Action
timer = EventSource.affEventSource \emitter -> do
  fiber <- forkAff $ forever do
    EventSource.emit emitter Timer

  pure $ EventSource.Finalizer do
    killFiber (error "Event source finalized") fiber
