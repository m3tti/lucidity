module View.DreamDiary where

import Prelude
import Data.Maybe (Maybe(..))

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Core as HC
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import DOM.HTML.Indexed.InputType (InputType(..))

import Components as C

data View
  = EditEntry
  | NewEntry
  | DreamList

data Dreamtype
  = LucidDream
  | Dream
  | Nightmare

data EntryField
  = EntryTitle
  | EntryText
  | EntryDreamType

type DreamEntry =
  { title :: String
  , text :: String
  , dreamType :: Dreamtype
  }

newEntry :: DreamEntry
newEntry =
  { title: ""
  , text: ""
  , dreamType: Dream
  }

type Slot = forall q. H.Slot q Void Unit

type State =
  { dreamList :: Array DreamEntry
  , activeView :: View
  , entry :: DreamEntry
  }

data Action
  = ActivateView View
  | Back
  | Save
  | Clear EntryField
  | Set EntryField String

component :: forall q i o m. H.Component HH.HTML q i o m
component =
  H.mkComponent
    { initialState
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }

initialState :: forall i. i -> State
initialState _ =
  { dreamList: []
  , activeView: DreamList
  , entry: newEntry
  }

render :: forall m. State -> H.ComponentHTML Action () m
render s = case s.activeView of
  DreamList -> dreamList s
  EditEntry -> editEntry s
  NewEntry -> editEntry s

editEntry :: forall p. State -> HC.HTML p Action
editEntry s =
  HH.div
  [ HP.class_ $ H.ClassName "w-100 relative"]
  [ C.header $ C.SubViewHeaderAction
    "Dream Diary"
    (Just Back)
    { icon: "save"
    , action: Just Save
    }
  , C.content
    [ C.input
      { placeholder: "Title"
      , value: s.entry.title
      , type_: InputText
      , clear: Just $ Clear EntryTitle
      , onInput: \s -> Just $ Set EntryTitle s
      }
    , C.input
      { placeholder: "Text"
      , value: s.entry.text
      , type_: InputText
      , clear: Just $ Clear EntryText
      , onInput: \s -> Just $ Set EntryText s
      }
    ]
  ]

dreamList :: forall p. State -> HC.HTML p Action
dreamList s =
  HH.div
  [ HP.class_ $ H.ClassName "w-100 relative"]
  [ C.header $ C.ActionHeader "Dream Diary"
    { icon: "note_add"
    , action: Just $ ActivateView NewEntry
    }
  , C.content
    [ C.searchPill
    , HH.ul
      [ HP.class_ $ H.ClassName "" ]
      $ map renderItem s.dreamList
    ]
  ]
  where
    renderItem i =
      HH.li
      [ ]
      [ HH.text i.title ]

handleAction :: forall o m. Action -> H.HalogenM State Action () o m Unit
handleAction = case _ of
  ActivateView v ->
    H.modify_ (_ { activeView = v })

  Back ->
    H.modify_ (_ { activeView = DreamList })

  Save ->
    pure unit

  Set EntryTitle v ->
    H.modify_ \s -> s{ entry = s.entry{ title=v } }

  Set EntryText v ->
    pure unit

  Set EntryDreamType v ->
    pure unit

  Clear EntryTitle ->
    pure unit

  Clear EntryText ->
    pure unit

  Clear EntryDreamType ->
    pure unit
