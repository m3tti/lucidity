module View.WILDAlarm where

import Prelude
import Data.Maybe (Maybe(..))

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Core as HC
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP

import Components as C

type Slot = forall q. H.Slot q Void Unit

type State =
  { }

data Action
  = Back

component :: forall q i o m. H.Component HH.HTML q i o m
component =
  H.mkComponent
    { initialState
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }

initialState :: forall i. i -> State
initialState _ =
  {}

render :: forall m. State -> H.ComponentHTML Action () m
render s =
  HH.div
  [ ]
  [ C.header $ C.SimpleHeader "WILD Alarm Clock View"
  ]

handleAction :: forall o m. Action -> H.HalogenM State Action () o m Unit
handleAction = case _ of
  Back ->
    pure unit

