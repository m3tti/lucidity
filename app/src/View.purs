module View where

import Prelude

data View
  = DreamDiary
  | REMulator
  | DeltaSleep
  | WILDAlarm

instance showView :: Show View where
  show = case _ of
    DreamDiary -> "DreamDiary"
    REMulator -> "REMulator"
    DeltaSleep -> "DeltaSleep"
    WILDAlarm -> "WILD Alarm clock"

derive instance eqView :: Eq View

views :: Array View
views =
  [ DreamDiary
  , REMulator
  , DeltaSleep
  , WILDAlarm
  ]
