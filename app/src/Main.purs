module Main where

import Prelude

import Effect (Effect)
import App as App
import Halogen.Aff as HA
import Halogen.VDom.Driver (runUI)
import Cordova (deviceReady)

main :: Effect Unit
main = HA.runHalogenAff do
  b <- HA.awaitBody
  _ <- deviceReady
  runUI App.component unit b
