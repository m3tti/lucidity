module App (component) where

import Prelude
import Effect.Aff (Aff)

import Data.Maybe (Maybe(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Core as HC
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP

import Data.Symbol (SProxy(..))

import View.DreamDiary as DD
import View.REM as REM
import View.DeltaSleep as Delta
import View.WILDAlarm as WILD
import View (View(..))

type State =
  { navOpen :: Boolean
  , activeView :: View
  }

data Action
  = SetView View

type Slots =
  ( ddView :: DD.Slot
  , remView :: REM.Slot
  , deltaView :: Delta.Slot
  , wildView :: WILD.Slot
  )

_ddView :: SProxy "ddView"
_ddView = SProxy

_remView :: SProxy "remView"
_remView = SProxy

_deltaView :: SProxy "deltaView"
_deltaView = SProxy

_wildView :: SProxy "wildView"
_wildView = SProxy

component :: forall q i o m. H.Component HH.HTML q i o Aff
component =
  H.mkComponent
    { initialState
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }

initialState :: forall i. i -> State
initialState _ =
 { navOpen: false
 , activeView: DreamDiary
 }

render :: forall m. State -> H.ComponentHTML Action Slots Aff
render s =
  HH.div
  [ HP.class_ $ H.ClassName "relative w-100 h-100" ]
  [ HH.div
    [ ]
    [ case s.activeView of
        DreamDiary -> HH.slot _ddView unit DD.component {} absurd
        REMulator -> HH.slot _remView unit REM.component {} absurd
        DeltaSleep -> HH.slot _deltaView unit Delta.component {} absurd
        WILDAlarm -> HH.slot _wildView unit WILD.component {} absurd
    ]
  , renderNavBar s
  ]

renderNavBar :: forall p. State -> HC.HTML p Action
renderNavBar s =
  HH.div
  [ HP.class_ $ H.ClassName "fixed fl bottom-0 w-100 h-20" ]
  $ map item [ DreamDiary, REMulator, DeltaSleep, WILDAlarm ]

  where
    item v =
      HH.div
      [ HP.class_ $ H.ClassName "tc fl w-25 pa2 f2 z-1"
      , HE.onClick \_ -> Just $ SetView v
      ]
      [ HH.i
        [ HP.class_
          $ H.ClassName $ "material-icons"
                        <> if v == s.activeView then " purple" else ""]
        [ HH.text $ getIcon v]
      ]

    getIcon = case _ of
      DreamDiary -> "book"
      REMulator -> "highlight"
      DeltaSleep -> "hourglass_empty"
      WILDAlarm -> "alarm"

handleAction ∷ forall o m. Action → H.HalogenM State Action Slots o Aff Unit
handleAction = case _ of
  SetView v ->
    H.modify_ (_ { activeView = v })
