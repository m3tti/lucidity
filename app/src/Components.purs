module Components where

import Prelude
import Data.Maybe (Maybe)

import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Core as HC
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP

import DOM.HTML.Indexed.InputType (InputType(..))

data Header i
  = ActionHeader String { icon :: String, action :: Maybe i }
  | SimpleHeader String
  | SubViewHeader String (Maybe i)
  | SubViewHeaderAction String (Maybe i) { icon :: String, action :: Maybe i }


header :: forall p i. Header i -> HC.HTML p i
header = case _ of
  ActionHeader title a ->
    HH.div
    [ HP.class_ $ H.ClassName "fl fixed top-0 h3 w-100 b bg-black z-1" ]
    [ HH.div
      [ HP.class_ $ H.ClassName "fl w-80 pa2 f2 mt2 pe-none"
      ]
      [ HH.text title ]
    , HH.a
      [ HP.class_ $ H.ClassName "absolute fl f2 w-20 pa2 tr bottom-0"
      , HE.onClick \_ -> a.action
      ]
      [ HH.i
        [ HP.class_ $ H.ClassName "material-icons purple" ]
        [ HH.text a.icon ]
      ]
    ]
  SubViewHeader title back ->
    HH.div
    [ HP.class_ $ H.ClassName "fl fixed top-0 h3 w-100 b bg-black z-1" ]
    [ HH.a
      [ HP.class_ $ H.ClassName "fl f2 w-40 pt1 bottom-0"
      , HE.onClick \_ -> back
      ]
      [ HH.i
        [ HP.class_ $ H.ClassName "material-icons purple" ]
        [ HH.text "keyboard_arrow_left" ]
      ]
    , HH.div
      [ HP.class_ $ H.ClassName "fixed fl tc w-100 pa2 f3 pe-none" ]
      [ HH.text title ]
    ]
  SubViewHeaderAction title back a ->
    HH.div
    [ HP.class_ $ H.ClassName "fl fixed top-0 h3 w-100 b bg-black z-1" ]
    [ HH.a
      [ HP.class_ $ H.ClassName "mt3 fl f2 w-50 pt1 bottom-0"
      , HE.onClick \_ -> back
      ]
      [ HH.i
        [ HP.class_ $ H.ClassName "material-icons purple" ]
        [ HH.text "keyboard_arrow_left" ]
      ]
    , HH.a
      [ HP.class_ $ H.ClassName "mt3 fl tr f2 w-50 pt1 pr2 bottom-0"
      , HE.onClick \_ -> a.action
      ]
      [ HH.i
        [ HP.class_ $ H.ClassName "material-icons purple" ]
        [ HH.text a.icon ]
      ]
    , HH.div
      [ HP.class_ $ H.ClassName "fixed fl tc w-100 pa2 f2 mt2 pe-none" ]
      [ HH.text title ]
    ]
  SimpleHeader title ->
    HH.div
    [ HP.class_ $ H.ClassName "fl fixed top-0 h3 w-100 b bg-black z-1" ]
    [ HH.div
      [ HP.class_ $ H.ClassName "fl w-100 mt3 f2" ]
      [ HH.text title ]
    ]

content :: forall p i. Array (HC.HTML p i) -> HC.HTML p i
content c =
  HH.div
  [ HP.class_ $ H.ClassName "relative fl w-100 ph2 mt5 pt3" ]
  c

searchPill :: forall p i. HC.HTML p i
searchPill =
  HH.input
  [ HP.class_ $ H.ClassName "input-reset bg-moon-gray pa2 ba br-pill w-100 tc"
  , HP.placeholder "Search"
  ]

type Button i =
  { label :: String
  , action :: Maybe i
  }

button :: forall p i. Button i -> HC.HTML p i
button b =
    HH.a
    [ HP.class_ $ H.ClassName "fl w-100 ba br2 tc pa2 bg-animate hover-bg-purple hover-black purple bg-black"
    , HE.onClick \_ -> b.action
    ]
    [ HH.text b.label ]

list l =
  HH.ul
  [ HP.class_ $ H.ClassName "fl w-100" ]
  $ map item l.items

  where
    item i =
      HH.li
      [ HP.class_ $ H.ClassName "" ]
      []

type Input i =
  { placeholder :: String
  , value :: String
  , type_ :: InputType
  , clear :: Maybe i
  , onInput :: String -> Maybe i
  }
input :: forall p i. Input i -> HC.HTML p i
input { placeholder, value, type_, onInput, clear } =
  HH.div
  [ HP.class_ $ H.ClassName "relative fl w-100 bb h3" ]
  [ HH.input
    [ HP.class_ $ H.ClassName "absolute bottom-0 mb1 moon-gray input-reset bg-black pa2 bn b--moon-gray w-100 tl"
    , HP.placeholder placeholder
    , HP.value value
    , HP.type_ type_
    , HE.onValueInput onInput
    ]
  , showClear
  ]
  where
    showClear = case value of
      "" -> HH.text ""
      _ ->
        HH.i
          [ HP.class_ $ H.ClassName "absolute bottom-0 right-0 pa2 mb1 material-icons"
          , HE.onClick \_ -> clear ]
          [ HH.text "clear" ]

