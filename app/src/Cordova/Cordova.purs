module Cordova where

import Prelude
import Effect (Effect)
import Effect.Aff (Aff)
import Effect.Aff.Compat (EffectFnAff, fromEffectFnAff)

foreign import _deviceready :: EffectFnAff Unit
foreign import _alert :: String -> Effect Unit

deviceReady :: Aff Unit
deviceReady = fromEffectFnAff _deviceready


