module Cordova.Flashlight where

import Prelude
import Effect (Effect)
import Effect.Aff (Aff)
import Effect.Aff.Compat (EffectFnAff, fromEffectFnAff)

foreign import _available :: EffectFnAff Boolean
foreign import _switchOn :: EffectFnAff Unit
foreign import _switchOff :: EffectFnAff Unit
foreign import _isSwitchedOn :: Effect Boolean

available :: Aff Boolean
available = fromEffectFnAff _available

switchOn :: Aff Unit
switchOn = fromEffectFnAff _switchOn

switchOff :: Aff Unit
switchOff = fromEffectFnAff _switchOff

isSwitchedOn :: Effect Boolean
isSwitchedOn = _isSwitchedOn



