exports._available = function(onError, onSuccess) {
  window.plugins.flashlight.available(function(isAvailable) {
    onSuccess(isAvailable)
  });

  return function(cancelError, cancelerError, cancelerSuccess) {
  };
}

exports._switchOn = function(onError, onSuccess) {
  window.plugins.flashlight.switchOn(
    onSuccess,
    onError
  );

  return function(cancelError, cancelerError, cancelerSuccess) {
  };
}

exports._switchOff = function(onError, onSuccess) {
  window.plugins.flashlight.switchOff(
    onSuccess,
    onError
  );

  return function(cancelError, cancelerError, cancelerSuccess) {
  };
}

exports._isSwitchedOn = function() {
  var on = window.plugins.flashlight.isSwitchedOn();

  return on;
}
