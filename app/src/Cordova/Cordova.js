exports._deviceready = function (onError, onSuccess) {
    document.addEventListener("deviceready", function(event) {
      onSuccess();
    }, false);

    return function (cancelError, cancelerError, cancelerSuccess) {
    };
};

exports._alert = function(msg) {
  return function() {
    alert(msg);
  }
}

