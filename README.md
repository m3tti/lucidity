# Lucidity
Lucidity is a lucid dream app helper. Which comes packed with plenty of
usefull techniques like:
- Dream Diary
- Remulator (an emulator with similar functionality like the RemEE Sleep mask)
- Delta Sleep (playing Pink noise randomly as you are fully assleep and in deep
    sleep)
- WILD Alarm (an alarm clock with soothing sound to wake you up gently to get
    your wild done)

## Development
First of all you have to initialize the project. Make sure a version of npm is
installed. 
`make initialize`
This will give you the whole toolchain to work with the project. Keep in mind
that this will overwrite your current purescript compiler and spago version if
allready installed.

### Start Dev Server
The build and dev server is started by `make serve` the build and deploy to your
android phone is done via `make deploy`. It is also possible to start pscid if
installed with make pscid
